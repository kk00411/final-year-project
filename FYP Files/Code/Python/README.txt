All python code required for hypothesis testing.

ipynb file required for use in Google Colab.
py files for use elsewhere.

For the world bank datasets (WBHypTest, linregWB) change the name of the input file as needed.

All code and datasets used in the project are available here:

https://gitlab.eps.surrey.ac.uk/kk00411/final-year-project.git