/* Code for trimming all non world bank datasets to the core variables only. In order to imporve processing time for
   hypothesis testing. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/ctdjoinquart.csv'
	DBMS=CSV
	OUT=joinedData;
	GETNAMES=YES;
	GUESSINGROWS=MAX;

/* Trim the dataset by keeping all relevant fields only. */
data joinedData;
	set joinedData (keep = TotalSamplesAnnual year ukos mode destination purpose visits nights spend sample
	formatted_date latitude longitude quarterNum TotalSamples attacktype1 attacktype1_txt targtype1 targtype1_txt
	full_date TotalAttacksQuart TotalAttacksAnnual Numeric_code);
run;

/* Export the dataset into a CSV. */
proc export data= joinedData
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/completed/ctdjointrim.csv" 
  replace;
run;




/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/exchangesjoinquart.csv'
	DBMS=CSV
	OUT=joinedData;
	GETNAMES=YES;
	GUESSINGROWS=MAX;

data joinedData;
	set joinedData (keep = TotalSamplesAnnual year ukos mode destination purpose visits nights spend sample
	formatted_date latitude longitude quarterNum TotalSamples Date Price Open High Low Change__ Currency Numeric_code);
run;

/* Export the dataset into a CSV. */
proc export data= joinedData
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/completed/exchangesjointrim.csv"
  replace;
run;






/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/climatejoinquart.csv'
	DBMS=CSV
	OUT=joinedData;
	GETNAMES=YES;
	GUESSINGROWS=MAX;

data joinedData;
	set joinedData (keep = TotalSamplesAnnual year ukos mode destination purpose visits nights spend sample
	formatted_date latitude longitude quarterNum TotalSamples _ISO3 Temperature____Celsius_ Numeric_code);
run;

/*Sort by the temperature in order to find the absolute minimum value. */
PROC SORT DATA=joinedData out=joinedData;
  BY Temperature____Celsius_ ;
RUN ;

/*Add 30 to each temperature value to make them all greater than zero. */
data joinedData;
	set joinedData;
	temp = Temperature____Celsius_ + 30;
run;

/*Test to see if records have a temp < 0*/
/*
data test;
   set joinedDate(where = (temp < 0));
run;

*/
/* Export the dataset into a CSV. */
proc export data= joinedData
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/completed/climatejointrim.csv"
  replace;
run;