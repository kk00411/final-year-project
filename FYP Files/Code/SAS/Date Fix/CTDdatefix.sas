/* Fixing the date variables by converting them all to a format that can be read by the QGIS time series plugin. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';

/* Empty the work library in order to create memory for further datasets. */
PROC DATASETS LIB=WORK NOWARN NOLIST KILL; RUN;

/* Create the library comDS, where all fully processed datasets will be stored. */
libname comDS '/folders/myfolders';

/*Import the datasets from the CSV file. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/CTDdata.csv'
	DBMS=CSV
	OUT=CTDdata;
	GETNAMES=YES;
	guessingrows=max;
RUN;

/*Change all invalid dates such that they be valid, by translating the incorrect dates with correct ones. Assume that all entries that
took place on the 31st of a month actually took place on the 30th of the same month. */
data CTDdata;
	set CTDdata;
	 full_date= translate(full_date,'30/04/', '31/04/');
	 full_date= translate(full_date,'30/06/', '31/06/');
	 full_date= translate(full_date,'30/11/', '31/11/');
run;

/*Test to see if any incorrect date values remain */
/*
data test;
   set CTDdata(where=(full_date='31/04/'));
run;
*/
/*
data test2;
   set CTDdata(where=(full_date='31/06/'));
run;
*/
/*
data test3;
   set CTDdata(where=(full_date='31/11/'));
run;
*/

/* Export the dataset into a CSV file. */
proc export data= CTDdata
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/CTDdata.csv" 
  replace;
run;