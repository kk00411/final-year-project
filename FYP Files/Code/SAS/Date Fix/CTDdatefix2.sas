/* Fixing the date variables by converting them all to a format that can be read by the QGIS time series plugin. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';

/* Empty the work library in order to create memory for further datasets. */
PROC DATASETS LIB=WORK NOWARN NOLIST KILL; RUN;

/* Create the library comDS, where all fully processed datasets will be stored. */
libname comDS '/folders/myfolders';

/*Import the datasets from the CSV file. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/CTDdata.csv'
	DBMS=CSV
	OUT=CTDdata;
	GETNAMES=YES;
	guessingrows=max;
RUN;

/* Change all invalid dates of 0 to 1. Assumption made that these records occured on the first of the month*/
data CTDdata;
	set CTDdata;
	 iday= translate(iday,'1', '0');
run;

/*Test to see if any incorrect date values remain */
/*
data test;
   set CTDdata(where=(iday=0));
run;
*/

/*data CTDdata;
	set CTDdata;
	 full_date= translate(full_date,'30/04/', '31/04/');
	 full_date= translate(full_date,'30/06/', '31/06/');
	 full_date= translate(full_date,'30/11/', '31/11/');
run;*/

/* Translate invalid dates of the 31st to 30th making them valid for those respective months. */
data CTDdata;
	set CTDdata;
	if ((iday = '31') & (imonth = '04')) then
		iday = '30';
	
	if ((iday = '31') & (imonth = '06')) then
		iday = '30';
		
	if ((iday = '31') & (imonth = '09')) then
		iday = '30';	
	
	if ((iday = '31') & (imonth = '11')) then
		iday = '30';
run;

/*Test to see if any incorrect date values remain */
/*
data test;
   set CTDdata(where=(iday='31' & imonth = '04'));
run;
*/
/*
data test2;
   set CTDdata(where=(iday='31' & imonth = '04'));
run;
*/
/*
data test3;
   set CTDdata(where=(iday='31' & imonth = '04'));
run;
*/
/*
data test4;
   set CTDdata(where=(iday='31' & imonth = '04'));
run;
*/

/* Convert all attributed required for the full date (day/month/year) from integers to strings. */
data CTDdata;
	set CTDdata;
	dayTemp = put(iday,z2.);
	format iday z2.;
	
	monthTemp = put(imonth, z2.);
	format imonth z2.;
	
	yearTemp = put(iyear, z4.);
run;

/*Concatanate all of the data into one single column seperated by '/'. */
data CTDdata;
	set CTDdata;
	full_date = catx('/',dayTemp, monthTemp ,yearTemp);
run;

/* Drop all of the temporary columns. */
data comDS.CTDdata;
	set CTDdata (drop = dayTemp monthTemp yearTemp);
run;

/* Export the dataset into a CSV file. */
proc export data= comDS.CTDdata
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/CTDdata.csv" 
  replace;
run;