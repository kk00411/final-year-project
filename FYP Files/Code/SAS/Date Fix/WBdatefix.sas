/* FIxing the date variables by converting them all to a format that can be read by the QGIS time series plugin. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';

/* Empty the work library in order to create memory for further datasets. */
PROC DATASETS LIB=WORK NOWARN NOLIST KILL; RUN;

/* Create the library comDS, where all fully processed datasets will be stored. */
libname comDS '/folders/myfolders';

/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/unemployment.csv'
	DBMS=CSV
	OUT=unemployment;
	GETNAMES=YES;
	guessingrows=max;
RUN;

/*Replace the _ in front of dates in order to put the dates into a format that can be understood by QGIS. */	
data unemployment;
	set unemployment;
	 year= translate(year,' ', '_');
run;

/* Export the dataset into a CSV. */
proc export data= unemployment
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/Unemployment.csv" 
  replace;
run;


/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/GDP.csv'
	DBMS=CSV
	OUT=GDP;
	GETNAMES=YES;
	guessingrows=max;
RUN;

/*Replace the _ in front of dates in order to put the dates into a format that can be understood by QGIS. */	
data GDP;
	set GDP;
	 year= translate(year,' ', '_');
run;

/* Export the dataset into a CSV. */
proc export data= GDP
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/GDP.csv" 
  replace;
run;




/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/GDPPP.csv'
	DBMS=CSV
	OUT=GDPPP;
	GETNAMES=YES;
	guessingrows=max;
RUN;

/*Replace the _ in front of dates in order to put the dates into a format that can be understood by QGIS. */	
data GDPPP;
	set GDPPP;
	 year= translate(year,' ', '_');
run;

/* Export the dataset into a CSV. */
proc export data= GDPPP
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/GDPPP.csv" 
  replace;
run;



/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/AgeRatio.csv'
	DBMS=CSV
	OUT=AgeRatio;
	GETNAMES=YES;
	guessingrows=max;
RUN;

/*Replace the _ in front of dates in order to put the dates into a format that can be understood by QGIS. */	
data AgeRatio;
	set AgeRatio;
	 year= translate(year,' ', '_');
run;

/* Export the dataset into a CSV. */
proc export data= AgeRatio
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/AgeRatio.csv" 
  replace;
run;



/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/AgeRatioYoung.csv'
	DBMS=CSV
	OUT=AgeRatioYoung;
	GETNAMES=YES;
	guessingrows=max;
RUN;
	
/*Replace the _ in front of dates in order to put the dates into a format that can be understood by QGIS. */	
data AgeRatioYoung;
	set AgeRatioYoung;
	 year= translate(year,' ', '_');
run;

/* Export the dataset into a CSV. */
proc export data= AgeRatioYoung
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/AgeRatioYoung.csv" 
  replace;
run;