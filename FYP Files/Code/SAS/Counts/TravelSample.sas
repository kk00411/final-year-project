/* Code for joining the total samples from the excel pivot tables onto each record in the set. Needed for hypothesis testing.
   Import and transpose the table in order to make it possible to then join onto the travel dataset.  */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/completed/travelTemp.csv'
	DBMS=CSV
	OUT=travel;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* If the sample value is null replace it with 1. It is assumed that each record must must a sample of a least 1. 
It is unknown whether these records have more than one sample. */
data travel;
	set travel;
	if(sample = '#NULL!') then
	sample = 1;
run;

/*Test to see if any null values remain */
/*
data test;
   set travel(where=(sample='#NULL'));
run;
*/

proc export data= travel
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/completed/allTravel.csv" 
  replace;
run;

/* Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/completed/travelSamples.csv'
	DBMS=CSV
	OUT=samples;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Transpose the year columns to now be rows of data */
proc transpose data=samples
	out = samples2;
	by ID country;
run;

/* Rename several columns to make it easier to understand what the data represents. Create seperate columns with the quarter and year
using the information in the _NAME_ column. */
data samples2;
	set samples2;
	drop id;
	rename COL1 = TotalSamples;
	yearT = substr(_NAME_, 4,4);
	quarterNumT = substr(_NAME_, 2,1);
	year = input(yearT, 8.);
	quarterNum = input(quarterNumT, 8.);
	drop _NAME_  yearT  quarterNumT;
run;


proc export data= samples2
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/completed/travelSamples2.csv" 
  replace;
run;

/* Join the samples data onto the travel dataset */
proc sql;
	create table joined as
	Select * from travel as a, samples2 as b
	where a.year = b.year AND a.quarterNum = b.quarterNum AND a.destination = b.country;
quit; 

/* Export into a csv */
proc export data= joined
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/completed/allTravel.csv" 
  replace;
run;


