/* Code for joining the total attacks from the excel pivot tables onto each record in the set. Needed for hypothesis testing.
   Import and transpose the table in order to make it possible to then join onto the travel dataset.  */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/CTDtempQuarter.csv'
	DBMS=CSV
	OUT=CTDcount;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* If the sample value is null replace it with 0. It is assumed that each record must must a sample of a least 1. 
It is unknown whether these records have more than one sample. */
data CTDcount;
   set CTDcount;
   array change _numeric_;
        do over change;
            if change=. then change=0;
        end;
 RUN;
 
 
 /* Transpose the year columns to now be rows of data */
proc transpose data=CTDcount
	out = CTDcount2;
	by country;
run;

/*Clean the data by renaming and dropping unwanted fields. */
data CTDcount2;
	set CTDcount2;
	rename COL1 = TotalAttacksQuart;
	yearT = substr(_NAME_, 4,4);
	quarterNumT = substr(_NAME_, 2,1);
	year = input(yearT, 8.);
	quarterNum = input(quarterNumT, 8.);
	drop _NAME_  yearT  quarterNumT;
run;

/* Export the dataset */
proc export data= CTDcount2
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/CTDrefQuarter.csv" 
  replace;
run;


/* Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/CTDdata.csv'
	DBMS=CSV
	OUT=CTD;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/*Join the two datasets. */
proc sql;
	create table joined as
	Select * from ctd as a, ctdcount2 as b
	where a.iyear = b.year AND a.quarter = b.quarterNum AND a.country_txt = b.country;
quit; 

data joined;
	set joined;
	drop year  quarterNum;
run;

/*Export to CSV */
proc export data= joined
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/CTDdata.csv" 
  replace;
run;