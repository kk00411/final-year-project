/* Join the travel data onto the CTD data. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/*Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/CTDdata.csv'
	DBMS=CSV
	OUT=ctddata;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN; 

/* Rename the Slovak Republic so that records in the travel data may be joined due to them being referred to as Slovakia. */
data ctd;
	set ctddata;
	if (country_txt = 'Slovak Republic') then
	country_txt = 'Slovakia';
run;

data ctd2;
	set ctd (keep = country_txt);
run;

/* Remove all duplicates to get a list of all countries represented */
proc sort Data = ctd2 NODUPKEY;
	BY country_txt;
run;


/* Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/reference.csv'
	DBMS=CSV
	OUT=reference;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/* Join the CTD data with the countries list */
proc sql;
	create table joined as
	Select * from ctd2 as a, reference as b
	where a.country_txt = b.destination;
quit; 

/* Check the two lists to ensure that all countries have been joined. That there are no further changes that need to be made. */
data reference2;
	set reference (keep = destination);
run;

data joined;
	set joined (keep = destination);
run;

/* Find all differences in the data to check to see if any countries are not represented. */
proc sql;
	delete from reference2 t1
	where exists (select * from joined t2 where t2.destination = t1.destination);
quit; 

/* Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/allTravel.csv'
	DBMS=CSV
	OUT=travel;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/* Convert all attributed required for the full date (day/month/year) from integers to strings. */
data CTD;
	set CTD;
	dayTemp = put(1, z2.);
	format iday z2.;
	
	monthTemp = put(imonth, z2.);
	format imonth z2.;
	
	yearTemp = put(iyear, z4.);
run;

/*Concatanate all of the data into one single column seperated by '/'. */
data CTD;
	set CTD;
	month_dateTemp = catx('/',dayTemp, monthTemp ,yearTemp);
	month_date = put(month_date,mmddyy10.);
	format month_date mmddyy10.;
run;

proc export data= ctd
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/CTDdata.csv" 
  replace;
run;

/* Drop all of the temporary columns. */
data CTD;
	set CTD (drop = dayTemp monthTemp yearTemp month_dateTemp);
run;

/* Join the two datasets */
proc sql;
	create table ctd3 as
	Select * from ctd as b, travel as a
	where a.destination = b.country_txt  AND a.formatted_date = b.month_date;
quit; 


proc export data= ctd3
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/CTDJoin.csv" 
  replace;
run;