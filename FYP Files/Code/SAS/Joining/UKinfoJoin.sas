/* File for joining the UK information onto each appropriate dataset for hypothesis testing. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/*Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/allTravel.csv'
	DBMS=CSV
	OUT=travel;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/*Import the dataset*/
PROC IMPORT DATAFILE= "/folders/myshortcuts/FYP_Files/completed/unemployment.csv"
	DBMS=CSV
	OUT=unemployment;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Filter the dataset to UK data only */
data UKinfo;	
	set unemployment (where = (Alpha_3_code = "GBR" ));
run;

/* Join the datasets */
proc sql;
	create table WHOjoin as
	Select * from travel as a, UKinfo as b
	where a.year = b.year;
quit; 


/*Trim the dataset and keep all relevant columns .*/
data WHOjoin;
	set WHOjoin (keep = TotalSamplesAnnual year ukos mode destination purpose visits nights spend sample formatted_date	latitude longitude 
	quarterNum TotalSamples	Country_Code Value date Numeric_code);
run;


/* Export the dataset into a CSV. */
proc export data= WHOJoin
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/unemploymentUK.csv" 
  replace;
run;










/*Import the dataset */
PROC IMPORT DATAFILE= "/folders/myshortcuts/FYP_Files/completed/GDP.csv"
	DBMS=CSV
	OUT=unemployment;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Filter the dataset to UK data only*/
data UKinfo;	
	set unemployment (where = (Alpha_3_code = "GBR" ));
run;

/* Join the datasets*/
proc sql;
	create table WHOjoin as
	Select * from travel as a, UKinfo as b
	where a.year = b.year;
quit; 


/*Trim the dataset and keep all relevant columns .*/
data WHOjoin;
	set WHOjoin (keep = TotalSamplesAnnual year ukos mode destination purpose visits nights spend sample formatted_date	latitude longitude 
	quarterNum TotalSamples	Country_Code Value date Numeric_code);
run;


/* Export the dataset into a CSV. */
proc export data= WHOJoin
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/GDPUK.csv" 
  replace;
run;







/*Import the dataset */
PROC IMPORT DATAFILE= "/folders/myshortcuts/FYP_Files/completed/GDPPP.csv"
	DBMS=CSV
	OUT=unemployment;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Filter to UK data only */
data UKinfo;	
	set unemployment (where = (Alpha_3_code = "GBR" ));
run;

/* Join the datasets */
proc sql;
	create table WHOjoin as
	Select * from travel as a, UKinfo as b
	where a.year = b.year;
quit; 


/*Trim the dataset and keep all relevant columns .*/
data WHOjoin;
	set WHOjoin (keep = TotalSamplesAnnual year ukos mode destination purpose visits nights spend sample formatted_date	latitude longitude 
	quarterNum TotalSamples	Country_Code Value date Numeric_code);
run;


/* Export the dataset into a CSV. */
proc export data= WHOJoin
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/GDPPPUK.csv" 
  replace;
run;









/*Import the dataset */
PROC IMPORT DATAFILE= "/folders/myshortcuts/FYP_Files/completed/climate2.csv"
	DBMS=CSV
	OUT=climate;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Filter to UK data only */
data UKinfo;	
	set climate (where = (_ISO3 = "GBR" ));
run;

/* Join the datasets */
proc sql;
	create table WHOjoin as
	Select * from travel as a, UKinfo as b
	where a.year = b._Year and a.quarterNum = b.quarter;
quit; 

/* Trim and keep relevant fields only*/
data WHOjoin;
	set WHOjoin (keep = TotalSamplesAnnual year ukos mode destination purpose visits nights spend sample
	formatted_date latitude longitude quarterNum TotalSamples _ISO3 Temperature____Celsius_);
run;

/* Sort the data */
PROC SORT DATA=WHOjoin out=WHOjoin;
  BY Temperature____Celsius_ ;
RUN ;

/* Add 30 to each temperature value such that they are all aboive 0 */
data WHOjoin;
	set WHOjoin;
	temp = Temperature____Celsius_ + 30;
run;

/*Test to see if any records have a climate less than zero */
/*
data test;
   set WHOjoin(where=(temp < 0));
run;

*/

/* Export the dataset into a CSV. */
proc export data= WHOJoin
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/climateUK.csv" 
  replace;
run;
