/* Join the datasets with the travel data based off of country, quarter and year. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/* Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/travelTemp.csv'
	DBMS=CSV
	OUT=travel;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/* Convert the the quarter to a numeric value that will remain constant between all datasets. */
data travel2;
	set travel;
	if quarter = 'Apr-Jun' then 
	quarterN = 2;
	
	if quarter = 'Jan-Mar' then 
	quarterN = 1;
	
	if quarter = 'Jul-Sep' then 
	quarterN = 3;
	
	if quarter = 'Oct-Dec' then 
	quarterN = 4;
	
	quarterNum = input(quarterN, best12.);
	drop quarterN;
run;

/*Test to see if records have a quarter number */
/*
data test;
   set travel2(keep=(quarterNum));
run;

PROC SORT DATA=test
 BY quarterNum;
 NODUPKEY ;
RUN ;

*/


/* Export the datasets. */
proc export data= travel2
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/travelTemp.csv" 
  replace;
run;

/* Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/allTravel.csv'
	DBMS=CSV
	OUT=travel2;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;




/* Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/CTDdata.csv'
	DBMS=CSV
	OUT=CTD;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Convert the the quarter to a numeric value that will remain constant between all datasets. */
data CTD2;
	set CTD;
	if (imonth = 0 | imonth = 1 | imonth = 2 | imonth = 3) then 
	quarter = 1;
	
	if (imonth = 4 | imonth = 5 | imonth = 6 ) then 
	quarter = 2;
	
	if (imonth = 7 | imonth = 8 | imonth = 9 ) then 
	quarter = 3;
	
	if (imonth = 10 | imonth = 11 | imonth = 12 ) then  
	quarter = 4;
run;

/*Test to see if records have a quarter number */
/*
data test2;
   set CTD2(keep=(quarter));
run;

PROC SORT DATA=test2
 BY quarter;
 NODUPKEY ;
RUN ;

*/

/* Due to memory concerns all unneeded columns must be removed from the dataset.*/
data ctd2;
	set ctd2 (keep = eventid iyear imonth country country_txt region region_txt latitude longitude attacktype1 
	attacktype1_txt targtype1 targtype1_txt full_date flag quarter );
run;

/* Export the dataset */
proc export data= ctd2
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/CTDdata.csv" 
  replace;
run;

/* Join the two datasets. */	
proc sql;
	create table ctdJoin as
	Select * from ctd2 as b, travel2 as a
	where a.destination = b.country_txt  AND a.quarterNum = b.quarter AND a.year = b.iYear;
quit; 

/* Export the dataset */
proc export data= ctdJoin
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/CTDJoinQuart.csv" 
  replace;
run;
		
		
		




/* Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/climate.csv'
	DBMS=CSV
	OUT=climate;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Convert the the quarter to a numeric value that will remain constant between all datasets. */
data climate2;
	set climate;
	if ((_statistics = 'Jan Average') | (_statistics = 'Feb Average') | (_statistics = 'Mar Average')) then 
	quarter = 1;
	
	if ((_statistics = 'Apr Average') | (_statistics = 'May Average') | (_statistics = 'Jun Average')) then  
	quarter = 2;
	
	if ((_statistics = 'Jul Average') | (_statistics = 'Aug Average') | (_statistics = 'Sep Average')) then  
	quarter = 3;
	
	if ((_statistics = 'Oct Average') | (_statistics = 'Nov Average') | (_statistics = 'Dec Average')) then  
	quarter = 4;
run;	

/*Test to see if records have a quarter number */
/*
data test2;
   set climate2(keep=(quarter));
run;

PROC SORT DATA=test2
 BY quarter;
 NODUPKEY ;
RUN ;

*/

/* Export the dataset */
proc export data= climate2
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/climate2.csv" 
  replace;
run;

/* Join the datasets. */	
proc sql;
	create table climateJoin as
	Select * from climate2 as b, travel2 as a
	where a.alpha_3_code = b._ISO3  AND a.quarterNum = b.quarter AND a.year = b._Year;
quit; 

/* Export the dataset */
proc export data= climateJoin
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/ClimateJoinQuart.csv" 
  replace;
run;
		
		
		
		
		
/* Import the dataset */		
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/allExchanges.csv'
	DBMS=CSV
	OUT=exchanges;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Convert the the quarter to a numeric value that will remain constant between all datasets. */
data exchanges2;
	set exchanges;
	
	monthTemp = month(Date);
	
	if ((monthTemp = 1) | (monthTemp = 2)  | (monthTemp = 3) ) then 
	quarter = 1;
	
	if ((monthTemp = 4) | (monthTemp = 5)  | (monthTemp = 6) ) then  
	quarter = 2;
	
	if ((monthTemp = 7) | (monthTemp = 8)  | (monthTemp = 9) ) then 
	quarter = 3;
	
	if ((monthTemp = 10) | (monthTemp = 11)  | (monthTemp = 12) ) then
	quarter = 4;
	
	drop monthTemp;
	
	year = year(Date);
run;	

/*Test to see if records have a quarter number */
/*
data test2;
   set exchanges2(keep=(quarter));
run;

PROC SORT DATA=test2
 BY quarter;
 NODUPKEY ;
RUN ;

*/

/* Export the dataset */
proc export data= exchanges2
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/exchanges2.csv" 
  replace;
run;

/* Join the datasets. */	
proc sql;
	create table exchangesJoin as
	Select * from exchanges2 as b, travel2 as a
	where a.country = b.country  AND a.quarterNum = b.quarter AND a.year = b.year;
quit; 

/* Export the dataset */
proc export data= exchangesJoin
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/exchangesJoinQuart.csv" 
  replace;
run;
		
		
	