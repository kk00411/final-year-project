Sources of datasets:

Climate Data: https://climateknowledgeportal.worldbank.org/download-data

CTD data: https://data.world/data-society/global-terrorism-data

Economic data: https://uk.investing.com/currencies/gbp-usd

TravelPac: https://www.ons.gov.uk/peoplepopulationandcommunity/leisureandtourism/datasets/travelpac

WorldBank: http://datatopics.worldbank.org/world-development-indicators/themes/economy.html
	   http://datatopics.worldbank.org/world-development-indicators/themes/people.html


countryCodes: https://datahub.io/core/currency-codes#resource-codes-all

countries: https://developers.google.com/public-data/docs/canonical/countries_csv

tableConvert: https://www.nationsonline.org/oneworld/country_code_list.htm#B

reference: https://www.nationsonline.org/oneworld/country_code_list.htm#B (no numeric column)


travelSamples/travelSamplesYR: Manually created using travel pac data. Pivot tables where the values are the total sum 
				of the samples per year.


CTDtempQuarter/CTDtempAnnual: Manually created using CTD data. Pivot tables where the values are the total sum 
				of the attacks per year.


ref: Manually created. List of all countries represented in travelPac




Order to run SAS code:

1) Initial Process files
2) Date fix files
3) Flagging files
4) Counts files
5) Joining files
6) Misc (can be run at anytime except for random which must always be run last)

(Run the R code on the world bank datasets before running them in SAS)

All code and datasets used in the project are available here:

https://gitlab.eps.surrey.ac.uk/kk00411/final-year-project.git