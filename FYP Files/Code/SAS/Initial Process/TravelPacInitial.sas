/* Initial processing for the travelpac datasets.
   Import each file and merge them into one. Further clean the data for mapping/further processing. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';

/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/* Create the library comDS, where all fully processed datasets will be stored. */
libname comDS '/folders/myfolders';

/* Macro to import all datasets within a certain directory into SAS. 

 Code taken from:
 https://documentation.sas.com/?docsetId=mcrolref&docsetTarget=n0ctmldxf23ixtn1kqsoh5bsgmg8.htm&docsetVersion=9.4&locale=en
 
 Author: SAS Institute.
 Date: 05/12/19
*/
%macro drive(dir,ext); *Have the directory and file type as parameters for the macro;
   %local cnt filrf rc did memcnt name; 
   %let cnt=0;          

   %let filrf=mydir;    
   %let rc=%sysfunc(filename(filrf,&dir)); 
   /*Create a macro variable named &DID that will use the DOPEN function to open the directory.
    A directory identifier value will be returned which if 0 means that the directory
    can not be opened. */
   %let did=%sysfunc(dopen(&filrf));
    %if &did ne 0 %then %do; /*Check that the directory was opened. */
   %let memcnt=%sysfunc(dnum(&did)); /*memcnt will return the number of members in a given directory.*/

    %do i=1 %to &memcnt;              
       /* Use the dread function to return the name of each file. and assign it to name */                
      %let name=%qscan(%qsysfunc(dread(&did,&i)),-1,.);                    
       /* Ensure that each file has a relevant extension else the %Do statement will not execute. */            
      %if %qupcase(%qsysfunc(dread(&did,&i))) ne %qupcase(&name) %then %do;
       %if %superq(&ext) = %superq(&name) %then %do;                         
          %let cnt=%eval(&cnt+1); *Import each file in the directory.;
          %put %qsysfunc(dread(&did,&i));  
          proc import datafile="&dir/%qsysfunc(dread(&did,&i))" out=dsn&cnt 
           dbms=csv replace; 
           GUESSINGROWS=MAX;
          run;          
       %end; 
      %end;  

    %end;
      %end;
  %else %put &dir cannot be open.; /*If the directory can't be opened print this to the log.*/
  %let rc=%sysfunc(dclose(&did)); /*Close the directory.*/     
             
 %mend drive;

/*Call the function.*/ 
%drive(/folders/myshortcuts/FYP_Files/TravelPac,csv) ;

/* Check the contents of the work directory. Put all set names into one dataset. */
proc contents
	data=work._all_ 
	out=work.allTravel(keep=memname) noprint;
run;

/* Sort the dataset and remove any duplicates. */
proc sort data=allTravel nodupkey;
	by memname;
run;  

/* Append all of the datasets in the work folder into one large dataset.
Code taken from: http://support.sas.com/kb/48/810.html
Author: SAS Institute
Date: 12/2019
*/

/* Create 2 macro variables - one with the names of each SAS data set 
and the other with the final count of the number of SAS data sets */
data _null_;
  set allTravel end=last;
  by memname;
  i+1;
  call symputx('name'||trim(left(put(i,8.))),memname);
  if last then call symputx('count',i);
run;

/* Remove the datasets that won't be appened from the list. */
data allTravel;
	set allTravel;
	if(MEMNAME = 'TEST' OR MEMNAME = 'ABCCONT' OR MEMNAME = 'ERCONT' OR MEMNAME = 'WORKCONT' OR MEMNAME = 'ALLEXRATES') then delete;
run;

/*Create the base set that all others will be appended onto. */
data allTravels;
	set work.dsn1;	
run;
/*Empty the dataset in order to avoid duplicate results. */
proc sql;
	DELETE FROM allTravels WHERE Year is NULL;
quit;

/* Macro containing the PROC APPEND that executes for each SAS data set you want to concatenate
together to create 1 SAS data set */
%macro combinesets;
	%do i=1 %to &count;
	proc append base=allTravels data=work.&&name&i force;
	run;
	%end;
%mend combinesets;

/*Execute the macro. */
%combinesets;

DATA allTravels;
	set allTravels (rename=(country=destination));
RUN;


/* Delete all blank rows in the dataset that arise from the appending of multiple datasets. */
proc sql;
	create table allTravel2 as
	select distinct *
	from allTravels;
quit;

/*Sort the data by the currency. */
PROC SORT data=allTravel2;
    BY Year;
RUN;


proc sql;
	DELETE FROM allTravel2 WHERE destination is NULL;
quit;


/*Sort thr dataset by the Country. */
PROC SORT data=allTravel2;
    BY destination Year;
RUN;

data allTravel2;
	set allTravel2;
	 year= compress(year,",");
run;
/* Reformat the dates within the datset. COnvert them to numeric values. */
data allTravel2;
	set allTravel2;
	
	if quarter="Jan-Mar" then
	fdate = "-01-01";
	else if quarter="Apr-Jun" then
	fdate = "-04-01";
	else if quarter="Jul-Sep" then
	fdate = "-07-01";
	else if quarter="Oct-Dec" then
	fdate = "-10-01";
run;

/*Test to see if all records have an fdate. */
/*
data test;
   set allTravel2(where=(fdate = NULL));
run;
*/


/*Concatanate all date fields to create one final one with the full date. */
Data allTravel2;
	Set allTravel2;
	formatted_date = cats(year,fdate);
Run;

data allTravel2;
	set allTravel2;
	 formatted_date = compress(formatted_date,",");
run;

Data allTravel2;
	Set allTravel2(DROP = fdate);
Run;


data test;
	set alltravel2 (keep = destination);
run;

/* Remove any duplicates as there should be none. */
proc sort data=test nodupkey;
	by destination;
run; 

proc contents data=allTravel2;
run;

/* Rename some countries to ensure that everything is kept uniform across all datasets. */
data allTravel2;
	set allTravel2;
	if destination = "Irish Republic"
	then destination = "Ireland";
	
	if destination = "China - Hong Kong"
	then destination = "Hong Kong";
	
	if destination = "China - Other"
	then destination = "China";
	
	if destination = "Cyprus EU"
	then destination = "Cyprus";
	
	if destination = "Cyprus Non EU"
	then destination = "Cyprus";
	
	if destination = "USA"
	then destination = "United States";
run;

/*Test to see if any records have an invalid country */
/*
data test2;
   set allTravel2(where=(destination = 'Irish Republic' |destination = 'China - Hong Kong'  |destination = 'China - Other'  
   |destination = 'Cyprus EU'  |destination = 'Vyprus Non EU' |destination = 'USA'));
run;
*/

/*Import the countries dataset with long and lat values. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/countries2.csv'
	DBMS=CSV
	OUT=Countries;
	GETNAMES=YES;
RUN;


/* Join the countries dataset onto the exchange rates dataset. */
PROC SQL;
	create table allTravel3 as
	Select * from allTravel2 as a, Countries as b
	where a.destination = b.name;
quit;


proc sql;
	create table UKdata as
	select country as startCountry, latitude as startLatitude, longitude as startLongitude, name as startName
	from work.Countries
	where country = "GB";
quit;

/* Ensure that the destination is not null. */
proc sql;
	create table allTravel4 as
	select * from allTravel3 as a, UKdata as b
	where a.destination IS NOT NULL;
quit;

data allTravel3;
	set allTravel3;
	if (sample = '#NULL!') then
	sample = '1';
run;

/*Test to see if any records have a null sample */
/*
data test2;
   set allTravel3(where=(sample = '#NULL!'));
run;
*/

/* Migrate the following datasets in to the completed datasets library. */
data comDS.allTravel;
	set allTravel3;
run;

/*Export the final dataset to a csv file. */
proc export data= comDS.allTravel
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/allTravel.csv" 
  replace;
run;

