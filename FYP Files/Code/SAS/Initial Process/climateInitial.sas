/* Initial processing of the climate data. Merge and clean the data for furthe rprocessing, mapping and hypothesis testing. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';

/* Empty the work library in order to create memory for further datasets. */
PROC DATASETS LIB=WORK NOWARN NOLIST KILL; RUN;

/* Create the library comDS, where all fully processed datasets will be stored. */
libname comDS '/folders/myfolders';

/* Macro to import all datasets within a certain directory into SAS. 

 Code taken from:
 https://documentation.sas.com/?docsetId=mcrolref&docsetTarget=n0ctmldxf23ixtn1kqsoh5bsgmg8.htm&docsetVersion=9.4&locale=en
 
 Author: SAS Institute.
 Date: 05/12/19
*/
%macro drive(dir,ext); *Have the directory and file type as parameters for the macro;
   %local cnt filrf rc did memcnt name; 
   %let cnt=0;          

   %let filrf=mydir;    
   %let rc=%sysfunc(filename(filrf,&dir)); 
   /*Create a macro variable named &DID that will use the DOPEN function to open the directory.
    A directory identifier value will be returned which if 0 means that the directory
    can not be opened. */
   %let did=%sysfunc(dopen(&filrf));
    %if &did ne 0 %then %do; /*Check that the directory was opened. */
   %let memcnt=%sysfunc(dnum(&did)); /*memcnt will return the number of members in a given directory.*/

    %do i=1 %to &memcnt;              
       /* Use the dread function to return the name of each file. and assign it to name */                
      %let name=%qscan(%qsysfunc(dread(&did,&i)),-1,.);                    
       /* Ensure that each file has a relevant extension else the %Do statement will not execute. */            
      %if %qupcase(%qsysfunc(dread(&did,&i))) ne %qupcase(&name) %then %do;
       %if %superq(&ext) = %superq(&name) %then %do;                         
          %let cnt=%eval(&cnt+1); *Import each file in the directory.;
          %put %qsysfunc(dread(&did,&i));  
          proc import datafile="&dir/%qsysfunc(dread(&did,&i))" out=dsn&cnt 
           dbms=csv replace;          
           GUESSINGROWS=MAX;
          run;          
       %end; 
      %end;  

    %end;
      %end;
  %else %put &dir cannot be open.; /*If the directory can't be opened print this to the log.*/
  %let rc=%sysfunc(dclose(&did)); /*Close the directory.*/     
             
 %mend drive;

/*Call the function.*/ 
%drive(/folders/myshortcuts/FYP_Files/Climate,csv) ;

/* Check the contents of the work directory. Put all set names into one dataset. */
proc contents
	data=work._all_ 
	out=work.allExRates(keep=memname) noprint;
run;

/* Sort the dataset and remove any duplicates. */
proc sort data=allExRates nodupkey;
	by memname;
run;  

/* Append all of the datasets in the work folder into one large dataset.
Code taken from: http://support.sas.com/kb/48/810.html
Author: SAS Institute
Date: 12/2019
*/

/* Create 2 macro variables - one with the names of each SAS data set 
and the other with the final count of the number of SAS data sets */
data _null_;
  set allExRates end=last;
  by memname;
  i+1;
  call symputx('name'||trim(left(put(i,8.))),memname);
  if last then call symputx('count',i);
run;

/* Remove the datasets that won't be appened from the list. */
data allExRates;
	set allExRates;
	if(MEMNAME = 'TEST' OR MEMNAME = 'ABCCONT' OR MEMNAME = 'ERCONT' OR MEMNAME = 'WORKCONT' OR MEMNAME = 'ALLEXRATES') then delete;
run;

/*Create the base set that all others will be appended onto. */
data allExchanges;
	set work.dsn1;	
run;
/*Empty the dataset in order to avoid duplicate results. */
proc sql;
	DELETE FROM allExchanges WHERE _year is NOT NULL;
quit;

/* Macro containing the PROC APPEND that executes for each SAS data set you want to concatenate
together to create 1 SAS data set */
%macro combinesets;
	%do i=1 %to &count;
	proc append base=allExchanges data=work.&&name&i force;
	run;
	%end;
%mend combinesets;

/*Execute the macro. */
%combinesets;

data allExchanges;
  set allExchanges;
  if missing(_Year) then delete;
run;

/* Delete all blank rows in the dataset that arise from the appending of multiple datasets. */
proc sql;
	create table allExchanges2 as
	select distinct *
	from allExchanges;
quit;

/* Reformat the dates within the datset. COnvert them to numeric values. */
data allExchanges2;
	set allExchanges2;
	
	if _statistics="Jan Average" then
	fdate = "-01-01";
	else if _statistics="Feb Average" then
	fdate = "-02-01";
	else if _statistics="Mar Average" then
	fdate = "-03-01";
	else if _statistics="Apr Average" then
	fdate = "-04-01";
	
	if _statistics="May Average" then
	fdate = "-05-01";
	else if _statistics="Jun Average" then
	fdate = "-06-01";
	else if _statistics="Jul Average" then
	fdate = "-07-01";
	else if _statistics="Aug Average" then
	fdate = "-08-01";
	
	if _statistics="Sep Average" then
	fdate = "-09-01";
	else if _statistics="Oct Average" then
	fdate = "-10-01";
	else if _statistics="Nov Average" then
	fdate = "-11-01";
	else if _statistics="Dec Average" then
	fdate = "-12-01";
run;

/*Concatanate all date fields to create one final one with the full date. */
Data allTravel2;
	Set allExchanges2;
	formatted_date = cats(_year,fdate);
Run;

Data allExchanges2;
	Set allTravel2(DROP = fdate);
Run;

/*Import the countries dataset with long and lat values. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/tableConvert.csv'
	DBMS=CSV
	OUT=Alpha3;
	GETNAMES=YES;
RUN;

/*Import the countries dataset with long and lat values. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/countries.csv'
	DBMS=CSV
	OUT=coordinates;
	GETNAMES=YES;
RUN;

proc sql;
	create table updatedConversion as 
	select * from Alpha3 as a, coordinates as b
	where a.Alpha_2_code = b.country;
quit;

proc sql;
	create table joinedTable as 
	select * from updatedConversion as a, allExchanges2 as b
	where a.Alpha_3_code = b._ISO3;
quit;

data joinedTable;
	set joinedTable (drop  = Country alpha_2_code alpha_3_code name numeric_code);
run;

/* Flag all records */
data joinedTable;
	set joinedTable;
	flag = 'T';
	if (_ISO3 = 'GBR') then
		flag = 'F';
run;

/*Test to see if all records have been flagged. */
/*
data test;
   set joinedTable(where=(flag = 'T'));
run;
*/
/*
data test2;
   set joinedTable(where=(flag = 'F'));
run;
*/

/*Export the final dataset to a csv file. */
proc export data= joinedTable
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/climate.csv" 
  replace;
run;

