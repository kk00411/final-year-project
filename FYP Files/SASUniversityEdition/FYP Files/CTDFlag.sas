/*Code for flagging all records in the file. T represents records from countries that are represented in the travel data
 F represents those that are not. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;


/*Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/reference.csv'
	DBMS=CSV
	OUT=reference;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/*Import the dataset */
PROC IMPORT DATAFILE= "/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/CTDdata.csv"
	DBMS=CSV
	OUT=unemployment;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Create a combined table. Entries in this dataset will be from countries that are represented in both the CTD and the travel data. */
PROC SQL;
	create table combine as
	Select * from reference as a, unemployment as b
	where a.destination = b.country_txt;
quit; 

/* Drop all unneeded columns */
data combine2;
	set combine (drop = alpha_2_code alpha_3_code destination);
run;

/* Add the relevant flag to these countries */
data flagged;
	set combine2;
	flag = 'T';
run;

data unemployment2;
	set unemployment;
run;

/* Remove all flagged records from the initial dataset leaving unflagged records only. */
proc sql;
	delete from unemployment2 t1
	where exists (select * from combine2 t2 where t2.country_txt = t1.country_txt and t2.VAR1 = t1.VAR1);
quit; 

/* Flag the remaining records with the appropriate flag. */
data unemployment2;
	set unemployment2;
	flag = 'F';
	if (country_txt = 'Slovak Republic') then
	flag = 'T';
run;

data MergeUnemployment;
	set flagged unemployment2;
run;


/*Export the final dataset to a csv file. */
proc export data= MergeUnemployment
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/CTDdata.csv" 
  replace;
run;
