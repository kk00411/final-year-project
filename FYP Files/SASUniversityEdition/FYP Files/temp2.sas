/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/*Import the countries dataset with long and lat values. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/allTravel.csv'
	DBMS=CSV
	OUT=travel;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/*Import the countries dataset with long and lat values. */
PROC IMPORT DATAFILE= "/folders/myshortcuts/FYP_Files/completed/ageratioyoung.csv"
	DBMS=CSV
	OUT=unemployment;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;



/* Join the datasets */
proc sql;
	create table travel3 as
	Select * from travel as a, unemployment as b
	where a.alpha_3_code = b.alpha_3_code AND a.year = b.year;
quit; 



proc export data= travel3
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/ageratioyoungJoin.csv" 
  replace;
run;