/* Fixing the date variables by converting them all to a format that can be read by the QGIS time series plugin. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';

/* Empty the work library in order to create memory for further datasets. */
PROC DATASETS LIB=WORK NOWARN NOLIST KILL; RUN;

/* Create the library comDS, where all fully processed datasets will be stored. */
libname comDS '/folders/myfolders';

/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/unemployment.csv'
	DBMS=CSV
	OUT=unemployment;
	GETNAMES=YES;
	guessingrows=max;
RUN;
	
/* Transpose the date columns into rows in order to make the set usable by QGIS. 
All fields referenced in the by statement are kept as fields and not transposed. */	
proc transpose data=Unemployment
	out = Unemployment2;
	By var1 country_name country_code indicator_name indicator_code Alpha_3_code latitude longitude;
run;

/*Rename the new fields. */
data comDS.Unemployment;
	set Unemployment2 (drop = Col2 col3 var1 indicator_code);
	rename _NAME_=year COL1=Value;
run;

/* Export the datasets.*/
proc export data= comDS.Unemployment
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/Unemployment.csv" 
  replace;
run;

/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/GDPPP.csv'
	DBMS=CSV
	OUT=GDPPP;
	GETNAMES=YES;
	guessingrows=max;
RUN;
	
/* Transpose the date columns into rows in order to make the set usable by QGIS. 
All fields referenced in the by statement are kept as fields and not transposed. */		
proc transpose data=GDPPP
	out = GDPPP2;
	By var1 country_name country_code indicator_name indicator_code Alpha_3_code latitude longitude;
run;

/*Rename the new fields. */
data comDS.GDPPP;
	set GDPPP2 (drop = Col2 col3 var1 indicator_code);
	rename _NAME_=year COL1=Value;
run;

/* Export the datasets.*/
proc export data= comDS.GDPPP
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/GDPPP.csv" 
  replace;
run;

/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/GDP.csv'
	DBMS=CSV
	OUT=GDP;
	GETNAMES=YES;
	guessingrows=max;
RUN;
	
/* Transpose the date columns into rows in order to make the set usable by QGIS. 
All fields referenced in the by statement are kept as fields and not transposed. */		
proc transpose data=GDP
	out = GDP2;
	By var1 country_name country_code indicator_name indicator_code Alpha_3_code latitude longitude;
run;

/*Rename the new fields. */
data comDS.GDP;
	set GDP2 (drop = Col2 col3 var1 indicator_code);
	rename _NAME_=year COL1=Value;
run;

/* Export the datasets.*/
proc export data= comDS.GDP
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/GDP.csv" 
  replace;
run;

/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/AgeRatioYoung.csv'
	DBMS=CSV
	OUT=AgeRatioYoung;
	GETNAMES=YES;
	guessingrows=max;
RUN;
	
/* Transpose the date columns into rows in order to make the set usable by QGIS. 
All fields referenced in the by statement are kept as fields and not transposed. */		
proc transpose data=AgeRatioYoung
	out = AgeRatioYoung2;
	By var1 country_name country_code indicator_name indicator_code Alpha_3_code latitude longitude;
run;

/*Rename the new fields. */
data comDS.AgeRatioYoung;
	set AgeRatioYoung2 (drop = Col2 col3 var1 indicator_code);
	rename _NAME_=year COL1=Value;
run;

/* Export the datasets.*/
proc export data= comDS.AgeRatioYoung
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/AgeRatioYoung.csv" 
  replace;
run;

/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/AgeRatio.csv'
	DBMS=CSV
	OUT=AgeRatio;
	GETNAMES=YES;
	guessingrows=max;
RUN;
	
/* Transpose the date columns into rows in order to make the set usable by QGIS. 
All fields referenced in the by statement are kept as fields and not transposed. */		
proc transpose data=AgeRatio
	out = AgeRatio2;
	By var1 country_name country_code indicator_name indicator_code Alpha_3_code latitude longitude;
run;

/*Rename the new fields. */
data comDS.AgeRatio;
	set AgeRatio2 (drop = Col2 col3 var1 indicator_code);
	rename _NAME_=year COL1=Value;
run;

/* Export the datasets.*/
proc export data= comDS.AgeRatio
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/AgeRatio.csv" 
  replace;
run;