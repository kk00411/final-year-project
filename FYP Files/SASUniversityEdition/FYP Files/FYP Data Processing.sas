/* SAS code to perform the preprocessing on the datasets needed for a GIS that measures tourism in and out of the UK.*/

dm 'clear log';

dm 'clear output';

FILENAME Country '/folders/myshortcuts/FYP_Files/Countries.xlsx';

PROC IMPORT DATAFILE=Country
	DBMS=XLSX
	OUT=WORK.Countries
	replace;
	GETNAMES=YES;
	
RUN;


FILENAME Data '/folders/myshortcuts/FYP_Files/2018.xlsx';

PROC IMPORT DATAFILE=Data
	DBMS=XLSX
	OUT=WORK.Data
	replace;
	GETNAMES=YES;
	
RUN;

DATA work.Data;
	set Work.Data (rename=(country=destination));
RUN;

PROC SQL;
	create table joined as
	Select * from work.Data as a, WORK.Countries as b
	where a.destination = b.name;
quit;

proc sql;
	create table UKdata as
	select country as startCountry, latitude as startLatitude, longitude as startLongitude, name as startName
	from work.Countries
	where country = "GB";
quit;

proc sql;
	create table fullData as
	select * from joined as a, UKdata as b
	where a.destination IS NOT NULL;
quit;


proc export data= fullData
  dbms=xlsx 
  outfile="/folders/myshortcuts/FYP_Files/fullData.xlsx" 
  replace;
run;
	
FILENAME DataV2 '/folders/myshortcuts/FYP_Files/fullDataV2.xlsx';

PROC IMPORT DATAFILE=DataV2
	DBMS=XLSX
	OUT=WORK.DataV2
	replace;
	GETNAMES=YES;
	
RUN;

data DataV2;
	set DataV2;
	
	if quarter="Jan-Mar" then
	formatted_date = "2018-01-01";
	else if quarter="Apr-Jun" then
	formatted_date = "2018-04-01";
	else if quarter="Jul-Sep" then
	formatted_date = "2018-07-01";
	else if quarter="Oct-Dec" then
	formatted_date = "2018-10-01";
run;

proc export data= DataV2
  dbms=xlsx 
  outfile="/folders/myshortcuts/FYP_Files/fullDataV2.xlsx" 
  replace;
run;
	
