/*Code to further clean the traveldata. Mainly getting rid of null values. Replacing them with the mean of all non-null values. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/allTravel.csv'
	DBMS=CSV
	OUT=travel;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Set all possible don't know values to D/K to keep them all uniform. */
data travel;
	set travel;
	if (sex = '#NULL!' | sex = 'Dont know' | sex = 'Don''t Know/') then
	sex = 'D/K';
run;

/* Replace the null values with the mean of all non null values. */
data travel;
	set travel;
	if (visits = '#NULL!') then
	visits = '2669.701048';
run;

/* Replace the null values with the mean of all non null values. */
data travel;
	set travel;
	if (nights = '#NULL!') then
	nights = '24052.91091';
run;

/* Replace the null values with the mean of all non null values. */
data travel;
	set travel;
	if (spend = '#NULL!') then
	spend = '1270362.926';
run;

/*Export the data into a CSV file */
proc export data= travel
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/allTravel.csv" 
  replace;
run;