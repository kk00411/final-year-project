/* Join the travel data onto the world bank data for hypothesis testing. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/*Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/allTravel.csv'
	DBMS=CSV
	OUT=travel;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/*Import the dataset */
PROC IMPORT DATAFILE= "/folders/myshortcuts/FYP_Files/completed/unemployment.csv"
	DBMS=CSV
	OUT=unemployment;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/*Import the dataset */
PROC IMPORT DATAFILE= "/folders/myshortcuts/FYP_Files/tableconvert.csv"
	DBMS=CSV
	OUT=tableconvert;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Add the data for the Channel Islands as it is not included.  */
data travel;
	set travel;
	if destination = 'Channel Islands' then
	country = 'XX';
run;

data tableconvert;
set tableconvert end=eof;
output;
if eof then do;

   country ='Channel Islands';

   alpha_2_code = 'XX';

   alpha_3_code = 'CHI';
   
   Numeric_code = 0;

   output;

  end;

run;

/* Join the two datasets to create a reference table. */
proc sql;
	create table travelled as
	Select * from travel as a, tableconvert as b
	where a.country = b.alpha_2_code;
quit; 


/* Drop all unneeded columns */
data travel2;
	set travelled (drop = numeric_code);
run;

/* Join the datasets */
proc sql;
	create table travel3 as
	Select * from travel2 as a, unemployment as b
	where a.alpha_3_code = b.alpha_3_code AND a.year = b.year;
quit; 

/* Export the datasets */
proc export data= travelled
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/travelTemp.csv" 
  replace;
run;


proc export data= travel3
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/UnemploymentJoin.csv" 
  replace;
run;





/*Import the dataset */
PROC IMPORT DATAFILE= "/folders/myshortcuts/FYP_Files/completed/ageRatio.csv"
	DBMS=CSV
	OUT=ageRatio;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Join and export the datasets */
proc sql;
	create table travel3 as
	Select * from travel2 as a, ageRatio as b
	where a.alpha_3_code = b.alpha_3_code AND a.year = b.year;
quit; 


proc export data= travel3
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/ageRatioJoin.csv" 
  replace;
run;






/*Import the dataset */
PROC IMPORT DATAFILE= "/folders/myshortcuts/FYP_Files/completed/ageRatioYoung.csv"
	DBMS=CSV
	OUT=ageRatioYoung;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/* Join and export the datasets */
proc sql;
	create table travel3 as
	Select * from travel2 as a, ageRatioYoung as b
	where a.alpha_3_code = b.alpha_3_code AND a.year = b.year;
quit; 


proc export data= travel3
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/ageRatioYoungJoin.csv" 
  replace;
run;






/*Import the dataset */
PROC IMPORT DATAFILE= "/folders/myshortcuts/FYP_Files/completed/GDP.csv"
	DBMS=CSV
	OUT=GDP;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/* Join and export the datasets */
proc sql;
	create table travel3 as
	Select * from travel2 as a, GDP as b
	where a.alpha_3_code = b.alpha_3_code AND a.year = b.year;
quit; 


proc export data= travel3
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/GDPJoin.csv" 
  replace;
run;




/*Import the dataset */
PROC IMPORT DATAFILE= "/folders/myshortcuts/FYP_Files/completed/GDPPP.csv"
	DBMS=CSV
	OUT=GDPPP;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/* Join and export the datasets */
proc sql;
	create table travel3 as
	Select * from travel2 as a, GDPPP as b
	where a.alpha_3_code = b.alpha_3_code AND a.year = b.year;
quit; 


proc export data= travel3
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/GDPPPJoin.csv" 
  replace;
run;










/*Import the dataset */
PROC IMPORT DATAFILE= "/folders/myshortcuts/FYP_Files/completed/allExchanges.csv"
	DBMS=CSV
	OUT=exchanges;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/* Join and export the datasets */
proc sql;
	create table travel3 as
	Select * from travel2 as a, exchanges as b
	where a.country = b.country AND a.formatted_date = b.date;
quit; 


proc export data= travel3
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/exchangesJoin.csv" 
  replace;
run;






/*Import the dataset */
PROC IMPORT DATAFILE= "/folders/myshortcuts/FYP_Files/completed/climate.csv"
	DBMS=CSV
	OUT=climate;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/* Join and export the datasets */
proc sql;
	create table travel3 as
	Select * from travel2 as a, climate as b
	where a.alpha_3_code = b._ISO3 AND a.formatted_date = b.formatted_date;
quit; 


proc export data= travel3
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/exchangesJoin.csv" 
  replace;
run;
