/* Code for trimming the world bank datasets to the useful variables only. Change the import file as needed. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/* Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/ageRatioJoin.csv'
	DBMS=CSV
	OUT=joinedData;
	GETNAMES=YES;
	GUESSINGROWS=MAX;

/*Trim the dataset and keep all relevant columns .*/
data joinedData;
	set joinedData (keep = TotalSamplesAnnual year ukos mode destination purpose visits nights spend sample	formatted_date	latitude longitude 
	quarterNum TotalSamples	Country_Code Value date Numeric_code);
run;

/* Export the dataset into a CSV. */
proc export data= joinedData
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/ageRatioJoin.csv" 
  replace;
run;






