/* Remove all commas from each dataset. Replace input file as needed. */
/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/ctdjoinquart.csv'
	DBMS=CSV
	OUT=joinedData;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Remove all commas from the dataset. 
	code taken from: https://stackoverflow.com/questions/39032909/sas-replace-character-in-all-columns */
data joinedData;
	set joinedData;
	
	array vars [*] _character_;
	
	do i = 1 to dim(vars);
		vars[i] = compress(tranwrd(vars[i], ",", ""));
	end;
	
	drop i;
run;

/*Export the dataset. */
proc export data= joinedData
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/ctdJoinQuart2.csv" 
  replace;
run;
