/*Initial processing of the world bank data. Import each file clean them for further use/processing. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';

/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/* Create the library comDS, where all fully processed datasets will be stored. */
libname comDS '/folders/myfolders';

/*Import the countries dataset with long and lat values. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/World Bank/AgeRatio.csv'
	DBMS=CSV
	OUT=AgeRatio;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/*Import the countries dataset with long and lat values. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/countries2.csv'
	DBMS=CSV
	OUT=Countries;
	GETNAMES=YES;
RUN;

/*Import the conversion table. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/tableConvert.csv'
	DBMS=CSV
	OUT=conversionTable;
	GETNAMES=YES;
RUN;

/*Create a table of countries from the conversion table that also appear in the countries set. */
PROC SQL;
	create table conversionTable2 as
	Select * from conversionTable as a, Countries as b
	where a.alpha_2_code = b.country;
quit;

/*Remove any unneeded columns. */
data conversionTable2;
	set conversionTable2 (keep = Alpha_3_code latitude longitude);
run;

/*Add in any missing data to the conversion table. In this case the data for the Channel Islands is inserted. */
data conversionTable2;
set conversionTable2 end=eof;
output;
if eof then do;

   alpha_3_code ='CHI';

   latitude = '49.214439';

   longitude = ' -2.13125';

   output;

  end;

run;

/* Join the country codes onto the age ratio dataset. */
PROC SQL;
	create table comDS.AgeRatioFinal as
	Select * from AgeRatio as a, conversionTable2 as b
	where a.country_code = b.alpha_3_code;
quit;

/*Export the dataset into a csv file. */
proc export data= comDS.AgeRatioFinal
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/ageRatio.csv" 
  replace;
run;



/*Import the age ratio young dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/World Bank/AgeRatioYoung.csv'
	DBMS=CSV
	OUT=AgeRatioYoung;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/*Join the two datasets together. */
PROC SQL;
	create table comDS.AgeRatioYoungFinal as
	Select * from AgeRatioYoung as a, conversionTable2 as b
	where a.country_code = b.alpha_3_code;
quit;

/* Export to a CSV file. */
proc export data= comDS.AgeRatioYoungFinal
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/ageRatioYoung.csv" 
  replace;
run;



/*Import the gross domestic product dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/World Bank/GDP.csv'
	DBMS=CSV
	OUT=GDP;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/*Join the teo datsets together. */
PROC SQL;
	create table comDS.GDPFinal as
	Select * from GDP as a, conversionTable2 as b
	where a.country_code = b.alpha_3_code;
quit;

/* Export the dataset into a CSV file. */
proc export data= comDS.GDPFinal
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/GDP.csv" 
  replace;
run;


/*Import the gross domestic product per person dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/World Bank/GDPPP.csv'
	DBMS=CSV
	OUT=GDPPP;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/* Join the two datasets together. */
PROC SQL;
	create table comDS.GDPPPFinal as
	Select * from GDPPP as a, conversionTable2 as b
	where a.country_code = b.alpha_3_code;
quit;

/* Export to a CSV file. */
proc export data= comDS.GDPPPFinal
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/GDPPP.csv" 
  replace;
run;



/*Import the unemployment dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/World Bank/unemployment.csv'
	DBMS=CSV
	OUT=unemployment;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/*Join the two datsets. */
PROC SQL;
	create table comDS.unemploymentFinal as
	Select * from unemployment as a, conversionTable2 as b
	where a.country_code = b.alpha_3_code;
quit;

/*Export the dataset. */
proc export data= comDS.unemploymentFinal
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/unemployment.csv" 
  replace;
run;