/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/allExchanges.csv'
	DBMS=CSV
	OUT=exchanges;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

data exchanges2;
	set exchanges;
	
	monthTemp = put(Date, monname.);
	
	if ((monthTemp = 'January') | (monthTemp = 'February')  | (monthTemp = 'March') ) then 
	quarter = 1;
	
	if ((monthTemp = 'April') | (monthTemp = 'May')  | (monthTemp = 'June') ) then  
	quarter = 2;
	
	if ((monthTemp = 'July') | (monthTemp = 'August')  | (monthTemp = 'September') ) then 
	quarter = 3;
	
	if ((monthTemp = 'October') | (monthTemp = 'November')  | (monthTemp = 'December') ) then
	quarter = 4;
	
	drop monthTemp;
run;	
