/* Creating random samples of the larger datasets for hypothesis testing. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/ctdjointrim.csv'
	DBMS=CSV
	OUT=dataset;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Assign each record in the set a random number. */
data random;
	set dataset;
	rand = ranuni(0);
run;

/*Sort the set by the random variable. Smallest numbers first. */
proc sort data=random out = random;
   by rand;
run;

/*Create a new set with the first 850000 records only. */
DATA cut;
    SET random (obs=850000);
RUN;



/* Export the dataset into a CSV. */
proc export data= cut
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/ctdRand.csv" 
  replace;
run;





/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/climatejointrim.csv'
	DBMS=CSV
	OUT=dataset;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/* Assign each record in the set a random number. */
data random;
	set dataset;
	rand = ranuni(0);
run;

/*Sort the set by the random variable. Smallest numbers first. */
proc sort data=random out = random;
   by rand;
run;

/*Create a new set with the first 850000 records only. */
DATA cut;
    SET random (obs=850000);
RUN;



/* Export the dataset into a CSV. */
proc export data= cut
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/climateRand.csv" 
  replace;
run;







/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/climateUK.csv'
	DBMS=CSV
	OUT=dataset;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/* Assign each record in the set a random number. */
data random;
	set dataset;
	rand = ranuni(0);
run;

/*Sort the set by the random variable. Smallest numbers first. */
proc sort data=random out = random;
   by rand;
run;

/*Create a new set with the first 850000 records only. */
DATA cut;
    SET random (obs=850000);
RUN;



/* Export the dataset into a CSV. */
proc export data= cut
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/climateUKRand.csv" 
  replace;
run;






/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/exchangesjointrim.csv'
	DBMS=CSV
	OUT=dataset;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/* Assign each record in the set a random number. */
data random;
	set dataset;
	rand = ranuni(0);
run;

/*Sort the set by the random variable. Smallest numbers first. */
proc sort data=random out = random;
   by rand;
run;

/*Create a new set with the first 850000 records only. */
DATA cut;
    SET random (obs=850000);
RUN;



/* Export the dataset into a CSV. */
proc export data= cut
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/exchangeRand.csv" 
  replace;
run;