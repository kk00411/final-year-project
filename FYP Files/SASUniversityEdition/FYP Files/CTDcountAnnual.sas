/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/* Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/CTDtempAnnual.csv'
	DBMS=CSV
	OUT=CTDcount;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;


/* If the sample value is null replace it with 0. It is assumed that each record must must a sample of a least 1. 
It is unknown whether these records have more than one sample. */
data CTDcount;
   set CTDcount;
   array change _numeric_;
        do over change;
            if change=. then change=0;
        end;
 RUN;
 
 
 
  /* Transpose the year columns to now be rows of data */
proc transpose data=CTDcount
	out = CTDcount2;
	by country;
run;

/* Clean the dataset by renaming and dropping unwanted fields. */
data CTDcount2;
	set CTDcount2;
	rename COL1 = TotalAttacksAnnual;
	yearT = substr(_NAME_, 2,4);
	year = input(yearT, 8.);
	drop _NAME_  yearT;
run;

proc export data= CTDcount2
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/CTDrefAnnual.csv" 
  replace;
run;


/* Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/CTD.csv'
	DBMS=CSV
	OUT=CTD;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Join the datasets. */
proc sql;
	create table joined as
	Select * from ctd as a, ctdcount2 as b
	where a.iyear = b.year AND a.country_txt = b.country;
quit; 

data joined;
	set joined;
	drop year;
run;

/* Export the dataset */
proc export data= joined
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/CTD.csv" 
  replace;
run;
 