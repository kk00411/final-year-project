/* Initial processing for the exchanges rate data. Import and merge all files. Further clean the data making it appropriate for use in mapping
, hypothesis testing or further processing. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';

/* Empty the work library in order to create memory for further datasets. */
PROC DATASETS LIB=WORK NOWARN NOLIST KILL; RUN;

/* Create the library comDS, where all fully processed datasets will be stored. */
libname comDS '/folders/myfolders';

/* Macro to import all datasets within a certain directory into SAS. 

 Code taken from:
 https://documentation.sas.com/?docsetId=mcrolref&docsetTarget=n0ctmldxf23ixtn1kqsoh5bsgmg8.htm&docsetVersion=9.4&locale=en
 
 Author: SAS Institute.
 Date: 05/12/19
*/
%macro drive(dir,ext); *Have the directory and file type as parameters for the macro;
   %local cnt filrf rc did memcnt name; 
   %let cnt=0;          

   %let filrf=mydir;    
   %let rc=%sysfunc(filename(filrf,&dir)); 
   /*Create a macro variable named &DID that will use the DOPEN function to open the directory.
    A directory identifier value will be returned which if 0 means that the directory
    can not be opened. */
   %let did=%sysfunc(dopen(&filrf));
    %if &did ne 0 %then %do; /*Check that the directory was opened. */
   %let memcnt=%sysfunc(dnum(&did)); /*memcnt will return the number of members in a given directory.*/

    %do i=1 %to &memcnt;              
       /* Use the dread function to return the name of each file. and assign it to name */                
      %let name=%qscan(%qsysfunc(dread(&did,&i)),-1,.);                    
       /* Ensure that each file has a relevant extension else the %Do statement will not execute. */            
      %if %qupcase(%qsysfunc(dread(&did,&i))) ne %qupcase(&name) %then %do;
       %if %superq(&ext) = %superq(&name) %then %do;                         
          %let cnt=%eval(&cnt+1); *Import each file in the directory.;
          %put %qsysfunc(dread(&did,&i));  
          proc import datafile="&dir/%qsysfunc(dread(&did,&i))" out=dsn&cnt 
           dbms=csv replace;          
           GUESSINGROWS=MAX;
          run;          
       %end; 
      %end;  

    %end;
      %end;
  %else %put &dir cannot be open.; /*If the directory can't be opened print this to the log.*/
  %let rc=%sysfunc(dclose(&did)); /*Close the directory.*/     
             
 %mend drive;

/*Call the function.*/ 
%drive(/folders/myshortcuts/SASUniversityEdition/FYP Files/Econ Data,csv) ;

/* Check the contents of the work directory. Put all set names into one dataset. */
proc contents
	data=work._all_ 
	out=work.allExRates(keep=memname) noprint;
run;

/* Sort the dataset and remove any duplicates. */
proc sort data=allExRates nodupkey;
	by memname;
run;  

/* Append all of the datasets in the work folder into one large dataset.
Code taken from: http://support.sas.com/kb/48/810.html
Author: SAS Institute
Date: 12/2019
*/

/* Create 2 macro variables - one with the names of each SAS data set 
and the other with the final count of the number of SAS data sets */
data _null_;
  set allExRates end=last;
  by memname;
  i+1;
  call symputx('name'||trim(left(put(i,8.))),memname);
  if last then call symputx('count',i);
run;

/* Remove the datasets that won't be appened from the list. */
data allExRates;
	set allExRates;
	if(MEMNAME = 'TEST' OR MEMNAME = 'ABCCONT' OR MEMNAME = 'ERCONT' OR MEMNAME = 'WORKCONT' OR MEMNAME = 'ALLEXRATES') then delete;
run;

/*Create the base set that all others will be appended onto. */
data allExchanges;
	set work.dsn1;	
run;
/*Empty the dataset in order to avoid duplicate results. */
proc sql;
	DELETE FROM allExchanges WHERE Date is NOT NULL;
quit;

/* Macro containing the PROC APPEND that executes for each SAS data set you want to concatenate
together to create 1 SAS data set */
%macro combinesets;
	%do i=1 %to &count;
	proc append base=allExchanges data=work.&&name&i force;
	run;
	%end;
%mend combinesets;

/*Execute the macro. */
%combinesets;

data allExchanges;
  set allExchanges;
  if missing(Date) then delete;
run;

/*data test;
  set allExchanges;
run;*/

*proc sql;
*	DELETE FROM test WHERE Open is NOT NULL;
*quit;

/* Delete all blank rows in the dataset that arise from the appending of multiple datasets. */
proc sql;
	create table allExchanges2 as
	select distinct *
	from allExchanges;
quit;

/*Sort the data by the currency. */
PROC SORT data=allExchanges2;
    BY Currency;
RUN;

*proc sql;
*	create table TESTSS as select * 
*	 FROM allExchanges2 WHERE Open IS NULL;
*quit;

*proc sql;
*	create table VND as select * 
*	 FROM allExchanges2 WHERE Currency = "VND";
*quit;

proc sql;
	DELETE FROM allExchanges2 WHERE Open is NULL;
quit;

/*proc sql;
	create table TESTSS as select * 
	 FROM allExchanges2 WHERE Open IS NULL;
quit;
*/

/* Import the country codes dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/CountryCodes.csv'
	DBMS=CSV
	OUT=CountryCodes;
	GETNAMES=YES;
RUN;

/*Drop any columns that are not needed. */
data CountryCodes;
	set CountryCodes (Keep = Entity Currency AlphabeticCode );
run;

/* Rename the currency column in order to avoid any future errors when joining datasets. */
data CountryCodes;
	set CountryCodes (rename = (Currency = CurrencyName));
run;
/* Join the exchange rates to the currencies using the country codes for each. */
proc sql;
	create table allExchanges3 as SELECT *
  	FROM allExchanges2 as a RIGHT JOIN CountryCodes as b  
    ON a.Currency = b.AlphabeticCode;
 quit;

/*Sort thr dataset by the entity. */
PROC SORT data=allExchanges3;
    BY Entity;
RUN;

/*Import the countries dataset with long and lat values. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/Countries.csv'
	DBMS=CSV
	OUT=Countries;
	GETNAMES=YES;
RUN;

/* Convert the dataset to all upercase so that it can be joined onto the exchange rates data. */
data CountriesCaps;
	set Countries;
	array vars(*) _character_;
	do i=1 to dim(vars);
		vars(i) = upcase(vars(i));
	end;
	drop i;
run;

/* Join the countries dataset onto the exchange rates dataset. */
proc sql;
	create table allExchanges4 as SELECT *
  	FROM allExchanges3 as a JOIN CountriesCaps as b  
    ON a.Entity = b.Name;
 quit;

proc sql;
	DELETE FROM allExchanges4 WHERE Open is NULL;
quit;

/* Migrate the following datasets in to the completed datasets library. */
data comDS.allExchanges;
	set allExchanges4;
run;

data comDS.Countries;
	set Countries;
run;

data comDS.CountriesCaps;
	set CountriesCaps;
run;

/* Export the final dataset to a csv file. */
proc export data= comDS.allExchanges
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/allExchanges.csv" 
  replace;
run;

