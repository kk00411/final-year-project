/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;



PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/allTravel.csv'
	DBMS=CSV
	OUT=travel2;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;








PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/CTD.csv'
	DBMS=CSV
	OUT=CTD;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Convert the the quarter to a numeric value that will remain constant between all datasets. */
data CTD2;
	set CTD;
	if (imonth = 0 | imonth = 1 | imonth = 2 | imonth = 3) then 
	quarter = 1;
	
	if (imonth = 4 | imonth = 5 | imonth = 6 ) then 
	quarter = 2;
	
	if (imonth = 7 | imonth = 8 | imonth = 9 ) then 
	quarter = 3;
	
	if (imonth = 10 | imonth = 11 | imonth = 12 ) then  
	quarter = 4;
run;

/* Due to memory concerns all unneeded columns must be removed from the dataset.*/
data ctd2;
	set ctd2 (keep = iyear imonth country country_txt latitude longitude attacktype1 
	attacktype1_txt targtype1 targtype1_txt full_date quarter totalattacksannual totalattacksquart);
run;

proc export data= ctd2
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/CTDsmall.csv" 
  replace;
run;

/* Join the two datasets. */	
proc sql;
	create table ctdJoin as
	Select * from travel2 as a, ctd2 as b
	where a.destination = b.country_txt  AND a.quarterNum = b.quarter AND a.year = b.iYear;
quit; 

proc export data= ctdJoin
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/CTDJoinQuart.csv" 
  replace;
run;
			
		