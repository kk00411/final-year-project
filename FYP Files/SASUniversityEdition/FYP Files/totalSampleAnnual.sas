/* Code for joining the total samples from the excel pivot tables onto each record in the set. Needed for hypothesis testing.
   Import and transpose the table in order to make it possible to then join onto the travel dataset.  */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/* Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/travelTempYR.csv'
	DBMS=CSV
	OUT=travel;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* If the sample value is null replace it with 1. It is assumed that each record must must a sample of a least 1. 
It is unknown whether these records have more than one sample. */
data travel;
	set travel;
	if(sample = '#NULL!') then
	sample = 1;
run;

/* Transpose the year columns to now be rows of data */
proc transpose data=travel
	out = travel;
	by country;
run;

/*Clean the dataset by renaming and dropping unneeded fields. */
data travel2;
	set travel;
	rename COL1 = TotalSamplesAnnual;
	yearT = substr(_NAME_, 2,4);
	year = input(yearT, 8.);
	drop _NAME_  yearT;
run;

/* Export into CSV. */
proc export data= travel2
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/travelSamplesAnnual.csv" 
  replace;
run;

/* Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/FYP_Files/completed/allTravel.csv'
	DBMS=CSV
	OUT=samples;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;



/* Join the samples data onto the travel dataset */
proc sql;
	create table joined as
	Select * from travel2 as b, samples as a
	where a.year = b.year AND a.destination = b.country;
quit; 

/* Export into a csv */
proc export data= joined
  dbms=csv
  outfile="/folders/myshortcuts/FYP_Files/completed/allTravel.csv" 
  replace;
run;