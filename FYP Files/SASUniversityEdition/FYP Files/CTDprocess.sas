/* Code for initial processing of the CTD data merge and clean the datasets for further processing, mapping and testing.  */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';

/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;

/* Create the library comDS, where all fully processed datasets will be stored. */
libname comDS '/folders/myfolders';

/* Macro to import all datasets within a certain directory into SAS. 

 Code taken from:
 https://documentation.sas.com/?docsetId=mcrolref&docsetTarget=n0ctmldxf23ixtn1kqsoh5bsgmg8.htm&docsetVersion=9.4&locale=en
 
 Author: SAS Institute.
 Date: 05/12/19
*/
%macro drive(dir,ext); *Have the directory and file type as parameters for the macro;
   %local cnt filrf rc did memcnt name; 
   %let cnt=0;          

   %let filrf=mydir;    
   %let rc=%sysfunc(filename(filrf,&dir)); 
   /*Create a macro variable named &DID that will use the DOPEN function to open the directory.
    A directory identifier value will be returned which if 0 means that the directory
    can not be opened. */
   %let did=%sysfunc(dopen(&filrf));
    %if &did ne 0 %then %do; /*Check that the directory was opened. */
   %let memcnt=%sysfunc(dnum(&did)); /*memcnt will return the number of members in a given directory.*/

    %do i=1 %to &memcnt;              
       /* Use the dread function to return the name of each file. and assign it to name */                
      %let name=%qscan(%qsysfunc(dread(&did,&i)),-1,.);                    
       /* Ensure that each file has a relevant extension else the %Do statement will not execute. */            
      %if %qupcase(%qsysfunc(dread(&did,&i))) ne %qupcase(&name) %then %do;
       %if %superq(&ext) = %superq(&name) %then %do;                         
          %let cnt=%eval(&cnt+1); *Import each file in the directory.;
          %put %qsysfunc(dread(&did,&i));  
          proc import datafile="&dir\%qsysfunc(dread(&did,&i))" out=dsn&cnt 
           dbms=csv replace;      
           GUESSINGROWS=MAX;
          run;          
       %end; 
      %end;  

    %end;
      %end;
  %else %put &dir cannot be open.; /*If the directory can't be opened print this to the log.*/
  %let rc=%sysfunc(dclose(&did)); /*Close the directory.*/     
             
 %mend drive;

/*Call the function.*/ 
/*%drive(/folders/myshortcuts/SASUniversityEdition/FYP Files/CTD/,csv) ;*/


/* Import the three seperate CTD datasets. */
PROC IMPORT DATAFILE='/folders/myshortcuts/SASUniversityEdition/FYP Files/CTD/gtd1.csv'
	DBMS=CSV
	OUT=WORK.DSN1;
	GETNAMES=YES;
RUN;

PROC IMPORT DATAFILE='/folders/myshortcuts/SASUniversityEdition/FYP Files/CTD/gtd2.csv'
	DBMS=CSV
	OUT=WORK.DSN2;
	GETNAMES=YES;
RUN;

PROC IMPORT DATAFILE='/folders/myshortcuts/SASUniversityEdition/FYP Files/CTD/gtd3.csv'
	DBMS=CSV
	OUT=WORK.DSN3;
	GETNAMES=YES;
RUN;

/* Check the contents of the work directory. Put all set names into one dataset. */
proc contents
	data=work._all_ 
	out=work.CTDdata(keep=memname) noprint;
run;

/* Sort the dataset and remove any duplicates. */
proc sort data=CTDdata nodupkey;
	by memname;
run;  

/* Append all of the datasets in the work folder into one large dataset.
Code taken from: http://support.sas.com/kb/48/810.html
Author: SAS Institute
Date: 12/2019
*/

/* Create 2 macro variables - one with the names of each SAS data set 
and the other with the final count of the number of SAS data sets */
data _null_;
  set CTDdata end=last;
  by memname;
  i+1;
  call symputx('name'||trim(left(put(i,8.))),memname);
  if last then call symputx('count',i);
run;

/* Remove the datasets that won't be appened from the list. */
data CTDdata;
	set CTDdata;
	if(MEMNAME = 'TEST' OR MEMNAME = 'ABCCONT' OR MEMNAME = 'ERCONT' OR MEMNAME = 'WORKCONT' OR MEMNAME = 'ALLEXRATES') then delete;
run;

/*Create the base set that all others will be appended onto. */
data CTDdata;
	set work.dsn1;	
run;

/* Macro containing the PROC APPEND that executes for each SAS data set you want to concatenate
together to create 1 SAS data set */
%macro combinesets;
	%do i=1 %to &count;
	proc append base=CTDdata data=work.&&name&i force;
	run;
	%end;
%mend combinesets;

/*Execute the macro. */
%combinesets;

/*Extract the full dates of each event by taking a substring (date only) of the first 10 characters in the
summary column. */
DATA comDS.CTDdata;
	Set CTDdata;
	full_date = Substr( Summary, 1, 10);

RUN; 

/* Export the final dataset into a CSV file. */
proc export data= comDS.CTDdata
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/CTDdata.csv" 
  replace;
run;
 