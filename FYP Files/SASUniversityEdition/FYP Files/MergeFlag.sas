/*Code for flagging all records in the file. T represents records from countries that are represented in the travel data
 F represents those that are not. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';
	
/* Empty the work library in order to create memory for further datasets. */
proc datasets library=WORK kill; 
run; 
quit;


/*Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/reference.csv'
	DBMS=CSV
	OUT=reference;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/*Import the dataset */
PROC IMPORT DATAFILE= "/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/Unemployment.csv"
	DBMS=CSV
	OUT=unemployment;
	GETNAMES=YES;
	GUESSINGROWS=MAX;
RUN;

/* Join the two datasets to determine which records must be flagged */
PROC SQL;
	create table combine as
	Select * from reference as a, unemployment as b
	where a.alpha_3_code = b.country_code;
quit; 

/* Drop all unneeded columns */
data combine2;
	set combine (drop = alpha_2_code destination);
run;

/* Flag all records */
data flagged;
	set combine2;
	flag = 'T';
run;

data unemployment2;
	set unemployment;
run;

/* Remove all flagged records from the initial dataset */
proc sql;
	delete from unemployment2 t1
	where exists (select * from combine2 t2 where t2.alpha_3_code = t1.alpha_3_code and t2.country_code = t1.country_code);
quit; 

/* Flag the remaining records */
data unemployment2;
	set unemployment2;
	flag = 'F';
run;

/* Merge the two flagged datasets */
data MergeUnemployment;
	set flagged unemployment2;
run;


/*Export the final dataset to a csv file. */
proc export data= MergeUnemployment
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/Unemployment.csv" 
  replace;
run;
