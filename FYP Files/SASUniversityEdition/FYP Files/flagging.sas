/*Code for flagging all records in the file. T represents records from countries that are represented in the travel data
 F represents those that are not. */

/*Clear the log and output windows before running the code. */
dm 'clear log';
dm 'clear output';

/* Empty the work library in order to create memory for further datasets. */
PROC DATASETS LIB=WORK NOWARN NOLIST KILL; RUN;

/* Create the library comDS, where all fully processed datasets will be stored. */
libname comDS '/folders/myfolders';

/* Import the dataset. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/allTravel.csv'
	DBMS=CSV
	OUT=travel;
	GETNAMES=YES;
	guessingrows=max;
RUN;

/* Keep the destination column only */
data travel;
	set travel (keep=destination);
run;

/* Remove all duplicates to get a list of all countries represented within the travel dataset */
proc sort data=travel nodupkey;
	by destination;
run;


proc contents data=travel;
run;

/* Rename some countries to ensure that everything is kept uniform across all datasets. */
data travel;
	set travel;
	if destination = "Irish Republic"
	then destination = "Ireland";
	
	if destination = "China - Hong Kong"
	then destination = "Hong Kong";
	
	if destination = "China - Other"
	then destination = "China";
	
	if destination = "Cyprus EU"
	then destination = "Cyprus";
	
	if destination = "Cyprus Non EU"
	then destination = "Cyprus";
	
	if destination = "USA"
	then destination = "United States";
run;

/*Import the dataset */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/countries2.csv'
	DBMS=CSV
	OUT=Countries;
	GETNAMES=YES;
RUN;


/* Join the countries dataset onto the exchange rates dataset. */
PROC SQL;
	create table travel2 as
	Select * from travel as a, Countries as b
	where a.destination = b.name;
quit;

data travel2;
	set travel2 (keep=destination country);
run;

/*Import the conversion table. */
PROC IMPORT DATAFILE= '/folders/myshortcuts/SASUniversityEdition/FYP Files/tableConvert.csv'
	DBMS=CSV
	OUT=conversionTable;
	GETNAMES=YES;
RUN;

/*Create a table of countries from the conversion table that also appear in the countries set. */
PROC SQL;
	create table conversionTable2 as
	Select * from conversionTable as a, travel2 as b
	where a.alpha_2_code = b.country;
quit;

data conversionTable2; 
	set conversionTable2 (drop= country Numeric_code);
run;

proc sort data=conversionTable2 nodupkey;
	by destination;
run;

/* Manually add the data for the Channel Islands as it is not included in the dataset */
data conversionTable2;
set conversionTable2 end=eof;
output;
if eof then do;

   alpha_3_code ='CHI';

   alpha_2_code ='CX';

   destination = 'Channel Islands';

   output;

  end;

run;

/*Export the final dataset to a csv file. */
proc export data= conversionTable2
  dbms=csv
  outfile="/folders/myshortcuts/SASUniversityEdition/FYP Files/completed/reference.csv" 
  replace;
run;

  