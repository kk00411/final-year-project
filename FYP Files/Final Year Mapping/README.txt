README for the GIS.

Layers and Respective Datasets:
climate : climeate (weighted by temperature)
CTDdata : CTDdata
allTravel : allTravel (weighted by samples)
allExchanges: allExchanges (weighted by Value)
Unemployment: Unemployment (weighted by Value)
GDPPP: GDPPP (weighted by Value)
GDP: GDP (weighted by Value)
ne_10m_land: Used for the map

Legend: img of the Legend used

Plugins used:
timemanager (for time series)
freehand raster georeference (placing legend on the map)

Note due to size limitations the full map and All code and datasets used in the project are available here:

https://gitlab.eps.surrey.ac.uk/kk00411/final-year-project.git